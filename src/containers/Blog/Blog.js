import React, {useState, useEffect, Suspense} from 'react';

import AppContext from '../../contexts/AppContext';
import PostList from '../../components/PostList/PostList';
// import NewPost from '../../components/NewPost/NewPost';
import Navigation from '../../components/Navigation/Navigation';
import './Blog.css';
import axios from '../../axios';
import {Route, Redirect, Switch} from 'react-router-dom';


/*const AsyncComponent = asyncComponent(() => {
    return import('../../components/NewPost/NewPost')
})*/

const NewPost = React.lazy(() => import('../../components/NewPost/NewPost'))
const Blog = (props) => {

    const [posts, setPosts] = useState([]);
    const [currentId, setCurrentId] = useState(null);
    const [error, setError] = useState(false);
    const [verified, setVerified] = useState(false);

    useEffect(() => {
        loadOnStart().then();
    }, []);

    const loadOnStart = async () => {
        const data = await getPosts();
        setPosts(data);
    };

    const getPosts = async () => {
        const res = await axios.get('posts/').catch(err => setError(true));
        if (res) {
            return res.data.slice(0, 4).map((item, index) => {
                return {
                    ...item,
                    author: 'Mikolaj',
                    id: index,
                };
            });
        }
        return [];
    };

    const getPost = (id) => {
        const array = posts.filter(item => {
            return item.id === id;
        });
        return array.length ? array[0] : null;
    };

    const updateCurrentId = (id) => {
        setCurrentId(id);
    };

    const context = {
        posts,
        currentId,
        error,
        verified,
        updateCurrentId,
        getPost,
    };

    return (
        <AppContext.Provider value={context}>
            <div>
                <header>
                    <Navigation/>
                </header>
                <section>
                    <Switch>
                        <Route path='/' exact render={() => <h1>Home</h1>}/>
                        <Route path='/posts' component={PostList}/>
                        {verified
                            // ? (<Route path='/new-post' component={NewPost}/>)
                            ? (<Route path='/new-post' render={() => (
                                <Suspense fallback={<h1>loading...</h1>}>
                                    <NewPost/>
                                </Suspense>
                            )}/>)
                            : null}
                        <Route render={() => <h1>Not found</h1>}/>
                    </Switch>
                    <Redirect from='/' to="posts"/>
                </section>
                {!verified
                    ?
                    (
                        <section>
                            <button onClick={() => setVerified(true)}>
                                Verify
                            </button>
                        </section>
                    )
                    : null
                }
            </div>
        </AppContext.Provider>
    );
};

export default Blog;
