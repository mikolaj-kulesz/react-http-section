import React, {useContext} from 'react';
import AppContext from '../../contexts/AppContext';
import Post from '../Post/Post';
import {Route} from 'react-router';
import FullPost from '../FullPost/FullPost';

const PostList = (props) => {
    const context = useContext(AppContext);

    const postList = !context.error
        ? context.posts.map((item, index) => {
            return (<Post title={item.title}
                          author={item.author}
                          id={item.id}
                          clickHandler={(id) => context.updateCurrentId(id)}
                          key={index}/>);
        })
        : (<strong>Something went wrong! :(</strong>);

    return (
        <div>
            <section className="Posts">
                {postList}
            </section>
            <section>
                <Route path='/posts/:id' exact component={FullPost}/>
            </section>
        </div>
    );
};

export default PostList;
