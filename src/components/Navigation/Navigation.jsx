import React from 'react';
import './Navigation.module.css';
import {NavLink} from 'react-router-dom';

const Navigation = (props) => {
    return (
        <nav className="navigation">
            <ul className="navigation-list">
                <NavLink className="navigation-list-item"
                         exact
                         activeClassName="my-active"
                         to='/'>Home</NavLink>
                <NavLink className="navigation-list-item"
                         to='/posts'>Posts</NavLink>
                <NavLink className="navigation-list-item"
                         to='/new-post'>New post</NavLink>
                <NavLink className="navigation-list-item"
                         to={{
                             pathname: '/new-posts',
                             hash: '#submit',
                             search: '?quick-submit=true',
                         }}>Test parameters</NavLink>
            </ul>
        </nav>
    );
};

export default Navigation;
