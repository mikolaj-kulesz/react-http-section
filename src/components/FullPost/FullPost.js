import React, {useContext, useState, useEffect} from 'react';
import axios from 'axios'
import AppContext from '../../contexts/AppContext';
import './FullPost.css';

const FullPost = (props) => {
    // const and let

    const { id } = props.match.params
    const context = useContext(AppContext)
    const [currentPostObject, setCurrentPostObject] = useState(null)
    let post = <p>Please select a Post!</p>;


    // useState
    useEffect(() => {
        const postId = parseInt(id, 10)
        const obj = context.getPost(postId);
        setCurrentPostObject(obj)
    },[props.match.params.id])

    // methods
    const deletePost = async() => {
        await axios.delete('posts/' + id)
        alert('deletion success')
    };

    post = currentPostObject
        ?
        (
            <div className="FullPost">
                <h1>{currentPostObject.title}</h1>
                <p>{currentPostObject.body}</p>
                <div className="Edit">
                    <button className="Delete"
                            onClick={deletePost}>Delete</button>
                </div>
            </div>

        )
        : null

    return post
}

export default FullPost;
