import React, {useState} from 'react';
import './NewPost.css';
import axios from 'axios';
import {Redirect} from 'react-router-dom';

const NewPost = (props) => {
    const [title, setTitle] = useState('')
    const [content, setContent] = useState('')
    const [author, setAuthor] = useState('Max')
    const [submitted, setSubmitted] = useState(false)

    const postRequest = async () => {
        const post = {title, content, author};
        await axios.post('posts/', post);
        alert('SUCCESS')
        // setSubmitted(true) // one way
        // props.history.push('/posts') // second way
        props.history.replace('/posts') // third way
    };

    const redirect = submitted ? (<Redirect to="/posts"/>) : null;

    return (
        <div className="NewPost">
            {redirect}
            <h1>Add a Post</h1>
            <label>Title</label>
            <input type="text" value={title}
                   onChange={(event) => setTitle(event.target.value)}/>
            <label>Content</label>
            <textarea rows="4" value={content}
                      onChange={(event) => setContent(event.target.value)}/>
            <label>Author</label>
            <select value={author}
                    onChange={(event) => setAuthor(event.target.value)}>
                <option value="Max">Max</option>
                <option value="Manu">Manu</option>
            </select>
            <button onClick={postRequest}>Add Post</button>
        </div>
    );
};

export default NewPost;
