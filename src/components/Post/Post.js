import React from 'react';

import './Post.css';
import {withRouter} from 'react-router-dom';

const post = (props) => {

    const clickHandler = () => {
        props.clickHandler(props.id)
        props.history.push({
            pathname: `/posts/${props.id}`
        })
    }

    return (
        <article className="Post"
                 onClick={clickHandler}>
                <h1>{props.title}</h1>
                <div className="Info">
                    <div className="Author">{props.author}</div>
                </div>
        </article>
    )
}

export default withRouter(post);
