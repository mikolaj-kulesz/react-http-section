import React from 'react';

const AppContext = React.createContext({
    posts: [],
    currentId: null,
    error: false,
    verified: false,
    updateCurrentId: () => {},
    getPost: () => {}
});

export default AppContext;
